/**
 * Created by Babar on 2/23/2016.
 */

module.exports = function(io, socket, users){
    var Users = require("./../models/UserModels/UserModel");
    var UserFriends = require("./../models/UserModels/UserFriendsModel");
    var InviteFriends = require("./../models/UserModels/InviteFriendsModel");
    var AcceptedFriendRequestsPending = require("./../models/UserModels/AcceptedFriendRequestsPending");

    var sockets = io.sockets.sockets;

    socket.on("send_friend_request", function(data){
        for(var i=0; i<users.length; i++){
            if(users[i].user_id == data.invite_to){
                sockets[users[i].socket_id].emit("new_friend_request", data);
                break;
            }
        }
    })

    socket.on("verify_request_received", function(data){
        InviteFriends.update({ _id : data.request_id }, { pending_status : 0 }, function(a){});
    });

    socket.on("accepted_friend_request", function(data){
        var a = new AcceptedFriendRequestsPending(data);
        a.save(function(err, row){
            for(var i=0; i<users.length; i++){
                if(users[i].user_id == data.send_to){
                    if(sockets[users[i].socket_id]){
                        sockets[users[i].socket_id].emit("friend_req_accepted", data);
                    }
                    break;
                }
            }
        });
    });

    socket.on("verify_accepted_msg_rec", function(data){
        AcceptedFriendRequestsPending.remove(data, function(err){});
    })
}