/**
 * Created by Horizon Softech on 1/14/2016.
 * Author  : Babar Bilal
 **/

var Users = require("./../models/UserModels/UserModel");
var UserFriends = require("./../models/UserModels/UserFriendsModel");
var InviteFriends = require("./../models/UserModels/InviteFriendsModel");
var errors = require("./errors");

var funcs = {};

function addFriend(user_id, id_to_add){
    UserFriends.find({ user_id : user_id }, function(err2, rows2){
        if(!err2){
            if(rows2.length > 0){
                UserFriends.update({ _id : rows2[0]._id }, {
                    $addToSet : {
                        Friends : id_to_add
                    }
                }, function(err3, rows3){});
            }else{
                var a = new UserFriends({
                    user_id : user_id,
                    Friends : [id_to_add]
                });
                a.save(function(err3, rows3){});
            }
        }
    });
}

funcs.sendFriendRequest = function(req, res){
    var data = req.body;
    if(data.invite_by){
        if(data.invite_to){
            Users.find({ _id : data.invite_by }, function(uerr, urows){
                if(!uerr){
                    InviteFriends.find(data, function(err, rows){
                        if(rows.length == 0){
                            var a = new InviteFriends(data);
                            a.save(function(err, row){
                                if(!err){
                                    res.json({
                                        status : 1,
                                        request_id : row._id,
                                        invite_to : data.invite_to,
                                        invite_by : data.invite_by,
                                        f_name : urows[0].f_name,
                                        l_name : urows[0].l_name
                                    }).status(200);
                                }else{
                                    errors.insertError(res, 0, "  Internal server error");
                                }
                            });
                        }else{
                            res.json({
                                status : 1,
                                msg : "Request Already sent!"
                            }).status(200);
                        }
                    });
                }else{
                    errors.insertError(res, 0, "User not found!");
                }
            });
        }else{
            errors.insertError(res, 2, "No Invite To Found!");
        }
    }else{
        errors.insertError(res, 3, "No Invite By Found!");
    }
}

funcs.acceptFriendRequest = function(req, res){
    var data = req.body;
    if(data.request_id){
        InviteFriends.find({ _id : data.request_id }, function(err, rows){
            if(!err){
                if(rows.length > 0){
                    var InsertObj = {
                        user_id : rows[0].invite_by,
                        Friends : [rows[0].invite_to]
                    }
                    UserFriends.find({ user_id : rows[0].invite_by }, function(err2, rows2){
                        if(!err){
                            if(rows2.length > 0){
                                UserFriends.update({ _id : rows2[0]._id }, {
                                    $addToSet : {
                                        Friends : rows[0].invite_to
                                    }
                                }, function(err3, rows3){
                                    if(!err3){
                                        addFriend(rows[0].invite_to, rows[0].invite_by);
                                        InviteFriends.remove({ _id : data.request_id }, function(a,b){});
                                        Users.find({ _id : InsertObj.user_id }, function(uerr, urow){
                                            if(!uerr){
                                                var t = {};
                                                t.status = 1;
                                                t.data = {
                                                    user_id : urow[0]._id,
                                                    f_name : urow[0].f_name,
                                                    l_name : urow[0].l_name,
                                                    image : urow[0].image
                                                }
                                                res.json(t).status(200);
                                            }
                                        })
                                    }
                                });
                            }else{
                                var a = new UserFriends(InsertObj);
                                a.save(function(err3, rows3){
                                    if(!err3){
                                        InviteFriends.remove({ _id : data.request_id }, function(a,b){});
                                        addFriend(rows[0].invite_to, rows[0].invite_by);
                                        Users.find({ _id : InsertObj.user_id }, function(uerr, urow){
                                            if(!uerr){
                                                var t = {};
                                                t.status = 1;
                                                t.data = {
                                                    user_id : urow[0]._id,
                                                    f_name : urow[0].f_name,
                                                    l_name : urow[0].l_name,
                                                    image : urow[0].image
                                                }
                                                res.json(t).status(200);
                                            }
                                        })
                                    }else{
                                        res.json({
                                            status : 0,
                                            msg : "Internal server error!"
                                        })
                                    }
                                })
                            }
                        }else{
                            errors.insertError(res, 0, "Internal Server Error!");
                        }
                    });
                }else{
                    errors.insertError(res, 2, "Id not found!");
                }
            }else{
                errors.insertError(res, 0, "Internal server error!");
            }
        });
    }
}

funcs.declineFriendRequest = function(req, res){
    if(req.body.request_id){
        var id = req.body.request_id;
        InviteFriends.remove({ _id : id }, function(err){
            res.json({
                status : 1
            });
        });
    }else{
        errors.insertError(res, 0, "No user id provided");
    }
}

funcs.getFriendsList = function(req, res){
    if(req.body.user_id){
        console.log(req.body);
        UserFriends.find({ user_id : req.body.user_id }, function(err, rows){
            if(rows.length > 0){
                var cnt = rows[0].Friends.length;
                if(!err){
                    res.json(rows[0]);
                }
            }else{
                res.json([]);
            }
        })
    }else{
        errors.insertError(res, 0, "No user id provided");
    }
}

funcs.getFriendsRequests = function(req, res){
    if(req.body.user_id){
        InviteFriends.find({ invite_to : req.body.user_id }, function(err, rows){
            if(!err){
                var resData = { status : 1, requests : [] };
                var cnt = rows.length;
                if(rows.length > 0){
                    for(var i=0; i<rows.length; i++){
                        Users.find({ _id : rows[i].invite_by }, function(uerr, urow){
                            if(!uerr){
                                resData.requests.push({
                                    request_id : rows[0]._id,
                                    invite_to : rows[0].invite_to,
                                    invite_by : rows[0].invite_by,
                                    f_name : urow[0].f_name,
                                    l_name : urow[0].l_name,
                                    pending_status : rows[0].pending_status
                                });
                                if(--cnt == 0){
                                    res.json(resData).status(200);
                                }
                            }
                        })
                    }
                }else{
                    res.json([]);
                }
            }else{
                errors.insertError(res, 0, "Invalid user_id");
            }
        })
    }else{
        errors.insertError(res, 0, "No user id provided");
    }
}

funcs.deleteAllFriendsRequests = function(req, res){
    InviteFriends.remove({}, function(a,b){});
    res.json({
        status : 1
    });
}

funcs.deleteAllFriends = function(req, res){
    UserFriends.remove({}, function(a,b){});
    res.json({
        status : 1
    });
}

funcs.getFriends = function(req, res){
    UserFriends.find({}, function(err, rows){
        res.json(rows);
    })
}

funcs.getPFR = function(req, res){

}


module.exports = funcs;