/**
 * Created by Horizon Softech on 12/17/2015.
 * Author  : Babar Bilal
 **/
var mongoose = require("../../db_connect");
var Schema = mongoose.Schema;

var InviteFriendsSchema = Schema({
    invite_by : Schema.ObjectId,
    invite_to : Schema.ObjectId,
    is_accept_invite : { type : Number, default : 0 },
    invite_send_at : { type : Date, default : Date.now },
    invite_accept_at : { type : Date, default : Date.now },
    pending_status : { type : Number, default : 1 }
});

var InviteFriends = mongoose.model("InviteFriends", InviteFriendsSchema);

module.exports = InviteFriends;