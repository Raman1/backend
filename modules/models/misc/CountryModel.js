/**
 * Created by Horizon Softech on 12/17/2015.
 * Author  : Babar Bilal
 **/

var mongoose = require("../../db_connect");
var Schema = mongoose.Schema;

var CountrySchema = new Schema({
    country_code : String,
    country_name : String,
    timezone : String,
    country_iso : String,
    image : String
});

var Country = mongoose.model("Country", CountrySchema);

module.exports = Country;