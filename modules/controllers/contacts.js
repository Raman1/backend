/**
 * Created by Horizon Softech on 1/14/2016.
 * Author  : Babar Bilal
 **/
var Users = require("../models/UserModels/UserModel");
var UserFriends = require("../models/UserModels/UserFriendsModel");
var InviteFriends = require("../models/UserModels/InviteFriendsModel");
var errors = require("./errors");

var funcs = {};

function convertStringContactsToArray(contacts){
    var b = contacts.substr(contacts.indexOf("[")+1, contacts.indexOf("]")-1);
    b = b.split("},");
    var c = [];
    for(var i=0; i< 172; i++){
        var m = b[i]+"}";
        m = m.replace("\\","");
        c.push(JSON.parse(m));
    }
    return c;
}

funcs.syncContacts = function(req, res){
    var contacts;
    if(req.body.user_id){
        try{
            contacts = JSON.parse(req.body.data);
        }catch(e){
            if(req.body.data.constructor == String){
                contacts = convertStringContactsToArray(req.body.data);
            }else if(req.body.data.constructor == Array){
                contacts = req.body.data;
            }
        }
        console.log(contacts);
        var UserId = req.body.user_id;
        var resContacts = {};
        resContacts.onLaleoo = [];
        resContacts.count = 0;
        var fetchCount = contacts.length;
        var TempUserIds = [];

        UserFriends.find({ user_id : UserId }, function(errf, user_friends){
            if(!errf){
                for(var i=0; i<contacts.length; i++){
                    Users.find({ full_phone_number : contacts[i].full_phone_number }, function(err, rows){
                        if(!err){
                            fetchCount--;
                            if(rows.length > 0){
                                if(!(rows[0]._id == UserId)){
                                    resContacts.onLaleoo.push({
                                        user_id : rows[0]._id,
                                        country_code : rows[0].country_code,
                                        phone_number : rows[0].phone_number,
                                        friends : 0,
                                        username : rows[0].f_name + " " + rows[0].l_name,
                                        full_phone_number : rows[0].country_code + "" + rows[0].phone_number
                                    });
                                    TempUserIds.push(rows[0]._id.toString());
                                    resContacts.count++;
                                }
                            }
                        }
                        if(fetchCount == 0){
                            if(user_friends.length > 0){
                                var UserFriends = user_friends[0].Friends;
                                for(var k=0; k<TempUserIds.length; k++){
                                    if(UserFriends.indexOf(TempUserIds[k].toString()) >= 0){
                                        resContacts.onLaleoo[k].friends = 1;
                                    }
                                }
                            }

                            InviteFriends.find({ invite_by : UserId }, function(ierr, irows){
                                if(!ierr){
                                    if(irows.length > 0){
                                        for(var k=0; k<irows.length; k++){
                                            var idx = TempUserIds.indexOf(irows[k].invite_to.toString());
                                            if(idx >= 0){
                                                resContacts.onLaleoo[idx].friends = 2;
                                            }
                                        }
                                        res.json(resContacts);
                                    }else{
                                        res.json(resContacts);
                                    }
                                }else{
                                    errors.insertError(res, 0, "Invite Friends error!");
                                }
                            })

                            /*for(var k=0; k<resContacts.onLaleoo[k].length; k++){
                             if(!(resContacts.onLaleoo[k].friends == 1)){

                             }
                             }*/
                        }
                    });
                }
            }else{
                errors.insertError(res,0, "Not a valid user id");
            }
        })
    }else{
        errors.insertError(res, 0, "No user id provider!");
    }
}

module.exports = funcs;