/**
 * Created by Horizon Softech on 11/19/2015.
 */

module.exports = function(grunt) {

    grunt.initConfig({

        // configure nodemon
        nodemon: {
            dev: {
                script: 'app.js'
            }
        },

        concurrent: {
            options: {
                logConcurrentOutput: true
            },
            tasks: ['nodemon', 'watch']
        },

        watch: {
            js: {
                files: ['website/app/**/*.js'],
                tasks: ['jshint', 'uglify']
            }
        },

        jshint: {
            all: ['website/app/**/*.js']
        },

        // take all the js files and minify them into app.min.js
        uglify: {
            build: {
                files: {
                    'admin/data/js/app.min.js': [
                        'bower_components/angular/angular.js',
                        'bower_components/angular-route/angular-route.js',
                        'bower_components/angular-ui-router/release/angular-ui-router.js',
                        'bower_components/ng-file-upload/ng-file-upload.js',
                        'bower_components/angular-animate/angular-animate.js',
                        'bower_components/angular-loading-bar/src/loading-bar.js',
                        'bower_components/angular-aria/angular-aria.js',
                        'bower_components/angular-bootstrap/ui-bootstrap.js',
                        'bower_components/angular-resource/angular-resource.js',
                        'bower_components/angular-sanitize/angular-sanitize.js',
                        'bower_components/angular-cookies/angular-cookies.js',
                        'admin/app/app.js',
                        'admin/app/**/*.js',
                        'admin/app/**/**/*.js'
                    ]
                }
            }
        }


    });

    // load nodemon
    grunt.loadNpmTasks('grunt-nodemon');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-concurrent');

    // register the nodemon task when we run grunt
    grunt.registerTask('default', ['nodemon', 'jshint', 'uglify', 'watch', 'concurrent']);

};