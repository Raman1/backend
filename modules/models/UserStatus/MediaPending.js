/**
 * Created by NGTricks
 * Author : Babar Bilal
 * Date : 4/3/2016
 **/

var mongoose = require("../../db_connect");
var Schema = mongoose.Schema;

var MediaPendingSchema = Schema({
    status_id : Schema.ObjectId,
    media_url : String,
    media_type : String,
    thumb_url : String,
    friend_id : Schema.ObjectId,
    t_user : Schema.ObjectId
});

var MediaPending = mongoose.model("MediaPending", MediaPendingSchema);

module.exports = MediaPending;