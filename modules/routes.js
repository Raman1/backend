/**
 * Created by NGTricks on 11/18/2015.
 * Author : Babar Bilal
 */

var express = require("express");
var routes = express.Router();

routes.use("/misc/", require("./routes/misc"));
routes.use("/users/", require("./routes/users"));
routes.use("/friends/", require("./routes/friends"));
routes.use("/status/", require("./routes/status"));
routes.get("/", function(req, res){
    //res.send('<script src="https://cdn.socket.io/socket.io-1.4.5.js"></script><script>var socket = io("http://api.laleoo.com:8080");socket.on("news", function (data) {console.log(data);socket.emit("my other event", { my: "data"});});</script>');
    res.send("Running");
});

module.exports = routes;