/**
 * Created by Horizon Softech on 1/14/2016.
 * Author  : Babar Bilal
 **/

var Country = require("./../models/misc/CountryModel");
var errors = require("./errors");

var funcs = {};
function alphabetical(a, b){
    var A = a.country_name.toLowerCase();
    var B = b.country_name.toLowerCase();
    if (A < B){
        return -1;
    }else if (A > B){
        return  1;
    }else{
        return 0;
    }
}

funcs.getCountriesList = function(req, res){
    Country.find({}, function(err, rows){
        if(!err){
            var data = {
                countries : []
            };
            for(var i=0; i<rows.length; i++){
                var t = rows[i].toObject();
                t.country_id = t._id;
                delete t._id;
                data.countries.push(t);
            }
            data.countries.sort(alphabetical);
            res.status(200).json(data);
        }else{
            errors.insertError(res, 0);
        }
    });
}

funcs.addCountry = function(req, res){
    var country = req.body;
    var t = country.length;
    Country.remove({}, function(a, b){
        for(var i=0; i<country.length; i++){
            var a = new Country({
                country_code : country[i].country_code,
                country_name : country[i].country_name,
                timezone : country[i].timezone,
                country_iso : country[i].country_iso,
                image : "countries/name.png"
            })
            a.save(function(err, row){
                t--;
                if(t == 0){
                    res.json({
                        status : 1
                    });
                }
            });
        }
    });
}

module.exports = funcs;