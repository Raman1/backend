/**
 * Created by Horizon Softech on 1/14/2016.
 * Author  : Babar Bilal
 **/

var users = require("./../models/UserModels/UserModel");
var UserFriends = require("./../models/UserModels/UserFriendsModel");
var UsersStatusList = require("./../models/UserStatus/UsersStatusListModel");
var UserStatus = require("./../models/UserStatus/UserStatusModel");
var InviteFriends = require("./../models/UserModels/InviteFriendsModel");
var errors = require("./errors");
var fs = require("fs");

var funcs = {};

function makeFileName(id, name){
    var nameExt = id + "." + name.split(".").reverse()[0];
    return nameExt;
}

funcs.signUp = function(req, res){
   
	console.log(req.body);
    if(req.body.email_address){
    	console.log("req.body.email_address");
        users.find({ email_address : req.body.email_address }, function(err, rows){
           if(rows.length > 0){
               errors.insertError(res, 2);
           }else{
               users.find({ country_code :  req.body.country_code, phone_number : req.body.phone_number }, function(err, rows){
                   if(rows.length > 0){
                       errors.insertError(res, 3);
                   }else{
                       req.body.full_phone_number = req.body.country_code + "" + req.body.phone_number;
                       var a = new users(req.body);
                       a.save(function(err, row){
                           if(!err){
                               var t = {};
                               t.status = 1;
                               t.user = row.toObject();
                               t.user.user_id = t.user._id;
                               delete t.user._id;
                               res.status(200).json(t);
                           }else{
                               errors.insertError(res, 0);
                           }
                       });
                   }
               });
           }
        });
    }else{
        errors.insertError(res, 0);
    }
}

funcs.login = function(req, res){
    if(req.body.email){
        users.find({ email_address : req.body.email }, function(err, rows){
            if(!err){
                if(rows.length > 0){
                    if(req.body.password){
                        if(rows[0].password == req.body.password){
                            var t = rows[0].toObject();
                            t.user_id = t._id;
                            delete t._id;
                            UserFriends.find({ user_id : t.user_id }, function(uferr, ufrows){
                                var userFriends = [];
                                var userFriendsStatus = [];
                                if(!uferr){
                                    if(ufrows.length > 0){
                                        var cntUF = ufrows[0].Friends.length;
                                        for(var j=0; j<ufrows[0].Friends.length; j++){
                                            users.find({ _id : ufrows[0].Friends[j] }, function(uerr, urows){
                                                if(urows.length){
                                                    userFriends.push({
                                                        f_name : urows[0].f_name,
                                                        l_name : urows[0].l_name,
                                                        user_id : urows[0]._id,
                                                        image : urows[0].image
                                                    })
                                                }
                                                if(--cntUF == 0){
                                                    getUserFriendsStatus(t.user_id, userFriends, t, res);
                                                }
                                            })
                                        }
                                    }else{
                                        getUserFriendsStatus(t.user_id, userFriends, t, res);
                                    }
                                }else{
                                    errors.insertError(res, 0, "Internal server error");
                                }
                            })
                        }else{
                            errors.insertError(res, 2, "Invalid Password");
                        }
                    }else{
                        errors.insertError(res, 2, "Empty Password");
                    }
                }else{
                    errors.insertError(res, 0, "Email not exists");
                }
            }else{
                errors.insertError(res, 9, "Internal Server Error");
            }
        })
    }else{
        errors.insertError(res, 9, "Invalid Email");
    }
}

funcs.getUsers = function(req, res){
    users.find({}, function(err, rows){
        for(var i=0; i<rows.length; i++){
            var t = rows[i].toObject();
            t.user_id = t._id;
            delete t._id;
            rows[i] = t;
        }
        res.json(rows);
    });
}

funcs.getUser = function(req, res){
    if(req.body.user_id){
        users.find({ _id : req.body.user_id }, function(err, rows){
            var t = rows[0].toObject();
            t.user_id = t._id;
            delete t._id;
            res.json(t);
        });
    }else{
        errors.insertError(res, 0, "No user_id found")
    }
}

funcs.uploadProfileImage = function(req, res){
    if(req.params.id){
        users.find({ _id : req.params.id }, function(err, row){
           if(!err){
               if(row.length > 0){
                   req.busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
                       var newFilename = makeFileName(req.params.id, filename);
                       fstream = fs.createWriteStream(req.baseURL + '/uploads/profile-images/' + newFilename);
                       file.pipe(fstream);
                       fstream.on('close', function () {
                           users.update({ _id : req.params.id }, {
                               image : '/profile-images/' + newFilename
                           }, function(err2, row2){
                               if(!err2){
                                   res.json({
                                       status : 1,
                                       filePath : '/profile-images/' + newFilename
                                   });
                               }else{
                                   res.json({
                                       status : 0,
                                       error : err2,
                                       msg : "Internal server error"
                                   });
                               }
                           })
                       });
                   });

                   req.busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
                       console.log('Field [' + fieldname + ']: value: ' + inspect(val));
                   });

                   req.busboy.on('finish', function() {
                       //console.log('Done parsing form!');
                   });
                   req.pipe(req.busboy);
               }else{
                   errors.insertError(res, 2);
               }
           }else{
               errors.insertError(res, 3);
           }
        });
    }else{
        errors.insertError(res, 0);
    }
}

funcs.removeUsers = function(req, res){
    users.remove({}, function(a,b){
        res.json({
            status : 1
        })
    });
}

funcs.removeUser = function(req, res){
    if(req.body.user_id){
        users.find({ _id : req.body.user_id }, function(err, rows){
            if(!err){
                if(rows.length > 0){
                    users.remove({ _id : req.body.user_id }, function(err){
                        if(!err){
                            fs.unlink(req.baseURL + '/uploads' + rows[0].image, function(err){
                                if(!err){
                                    res.json({
                                        status : 1,
                                        msg : "Image removed"
                                    })
                                }else{
                                    res.json({
                                        status : 1,
                                        msg : "Image not found",
                                        error : err
                                    })
                                }
                            })
                        }
                    });
                }else{
                    errors.insertError(res, 1, "User removed");
                }
            }else{
                errors.insertError(res, 0, "User not exists!");
            }
        })
    }else{
        errors.insertError(res, 0, "No user id provided");
    }
}

function getUserFriendsStatus(user_id, userFriends, t, res){
    UsersStatusList.find({ user_id : user_id }, function(serr, srows){
        if(!serr){
            if(srows.length > 0){
                var userFriendsStatus = [];
                var cntUF = srows[0].friends_status.length;
                for(var k=0; k<srows[0].friends_status.length; k++){
                    UserStatus.find({ _id : srows[0].friends_status[k] }, function(ferr, frows){
                        if(!ferr){
                            if(frows.length > 0){
                                // if(frows[0].is_active == 1){
                                    frows = frows[0].toObject();
                                    frows.status_id = frows._id;
                                    delete frows._id;
                                    userFriendsStatus.push(frows);
                                // }
                            }
                        }
                        if(--cntUF == 0){
                            getUserOwnStatus(user_id, {
                                status : 1,
                                data : t,
                                friends : userFriends,
                                friendsStatus : userFriendsStatus,
                                activeStatus : [],
                                personalStatus : [],
                                activeCount : 0
                            }, res);
                        }
                    });
                }
            }else{
                getUserOwnStatus(user_id, {
                    status : 1,
                    data : t,
                    friends : userFriends,
                    friendsStatus : [],
                    activeStatus : [],
                    personalStatus : []
                }, res);
            }
        }
    })
}

function getUserOwnStatus(user_id, resData, res){
    UserStatus.find({ user_id : user_id }, function(serr, srows){
        if(!serr){
            if(srows.length > 0){
                var currentActive = {};
                var personalStatuses = [];
                for(var m=0; m<srows.length; m++){
                    if(srows[m].is_active == 1){
                        var t = srows[m].toObject();
                        t.status_id = t._id;
                        delete t._id;
                        currentActive = t;
                        resData.activeCount = 1;
                    }else{
                        var t = srows[m].toObject();
                        t.status_id = t._id;
                        delete t._id;
                        personalStatuses.push(t);
                    }
                }
                resData.activeStatus = currentActive;
                resData.personalStatus = personalStatuses.sort(sortByCreated_At);
                getFriendsInvites(user_id, resData, res);
            }else{
                getFriendsInvites(user_id, resData, res);
            }
        }
    });
}

function getFriendsInvites(user_id, resData, res){
    InviteFriends.find({ invite_to : user_id }, function(err, rows){
        if(!err){
            if(rows.length > 0){
                var l = rows.length;
                resData.pendingFriendRequests = []
                for(var i=0; i<l; i++){
                    users.find({ _id : rows[i].invite_by }, function(uerr, urows){
                        if(!uerr){
                            resData.pendingFriendRequests.push({
                                f_name : urows[0].f_name,
                                l_name : urows[0].l_name,
                                request_id : urows[0]._id,
                                invite_by : urows[0]._id
                            });
                            if(--l == 0){
                                res.json(resData);
                            }
                        }
                    })
                }
            }else{
                resData.pendingFriendRequests = [];
                res.json(resData);
            }
        }
    })
}

function sortByCreated_At(a,b){
	var dateA = new Date(a.created_at).getTime();
	var dateB = new Date(b.created_at).getTime();
	return dateA > dateB ? 1 : -1; 
}

module.exports = funcs;