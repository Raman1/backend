/**
 * Created by Horizon Softech on 12/17/2015.
 * Author  : Babar Bilal
 **/

var mongoose = require("../../db_connect");
var Schema = mongoose.Schema;

var AcceptedFriendRequestsPendingSchema = Schema({
    user_id : Schema.ObjectId,
    send_to : Schema.ObjectId,
    f_name : String,
    l_name : String
});

var AcceptedFriendRequestsPending = mongoose.model("AcceptedFriendRequestsPending", AcceptedFriendRequestsPendingSchema);

module.exports = AcceptedFriendRequestsPending;