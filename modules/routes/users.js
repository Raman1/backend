/**
 * Created by Horizon Softech on 1/14/2016.
 * Author  : Babar Bilal
 **/

var express = require("express");
var Router = express.Router();
var users = require("./../controllers/users");

Router.post("/signUp", users.signUp);
Router.post("/login", users.login);
Router.post("/uploadProfileImage/:id", users.uploadProfileImage);
Router.get("/getUsers", users.getUsers);
Router.post("/getUser", users.getUser);
Router.get("/removeUsers", users.removeUsers);
Router.post("/removeUser", users.removeUser);

module.exports = Router;