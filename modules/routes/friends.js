/**
 * Created by Horizon Softech on 1/14/2016.
 * Author  : Babar Bilal
 **/

var express = require("express");
var Router = express.Router();
var friends = require("./../controllers/friends");

Router.post("/sendFriendRequest", friends.sendFriendRequest);
Router.post("/acceptFriendRequest", friends.acceptFriendRequest);
Router.post("/declineFriendRequest", friends.declineFriendRequest);
Router.post("/getFriendsList", friends.getFriendsList);
Router.post("/getFriendsRequests", friends.getFriendsRequests);
Router.get("/deleteAllFriendRequests", friends.deleteAllFriendsRequests);
Router.get("/deleteAllFriends", friends.deleteAllFriends);
Router.get("/getFriends", friends.getFriends);

module.exports = Router;