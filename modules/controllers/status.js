/**
 * Created by NGTricks on 1/14/2016.
 * Author  : Babar Bilal
 **/

var Users = require("./../models/UserModels/UserModel");
var UserFriends = require("./../models/UserModels/UserFriendsModel");
var InviteFriends = require("./../models/UserModels/InviteFriendsModel");
var UserStatus = require("./../models/UserStatus/UserStatusModel");
var PendingStatus = require("./../models/UserStatus/PendingStatusModel");
var UsersStatusList = require("./../models/UserStatus/UsersStatusListModel");
var MediaPending = require("./../models/UserStatus/MediaPending");
var errors = require("./errors");

var funcs = {};

funcs.createStatus = function(req, res){
    var data = req.body;
    if(data.user_id){
        if(data.hide_from){
            data.hide_from = JSON.parse(data.hide_from);
        }

        
        //De activate if there is an already active status available, 
        //If client miss the call "makeStatusDeActive" or "removeOldActive"
        //Server should do it by itself when creating new Status.
        // deActiveOldSatus(req);



        var a = new UserStatus(data);
        a.save(function(err, row){
            if(!err){
                var row = row.toObject();
                row.status_id = row._id;
                row.local_id = req.body.local_id;
                delete row._id;
                res.json(row);
            }else{
                errors.insertError(res, 0, "Status not created");
            }
        });

    }else{
        errors.insertError(res, 0, "No User Id Found!");
    }
}

funcs.removeOldActive = function(req, res){
    if(req.body.user_id){
        UserStatus.update({ user_id : req.body.user_id }, { is_active : 0 }, { multi : true }, function(err, row){
            if(!err){
                res.json({
                    status : 1
                });
            }
        })
    }else{
        errors.insertError(res, 0, "No User id provider");
    }
}

/**
Make Old status De-active
**/
funcs.makeStatusDeActive = function(req, res){
    if(req.body.status_id){
        UserStatus.update({ _id : req.body.status_id }, { is_active : 0 }, { multi : true }, function(err, row){
            if(!err){
                res.json({
                    status : 1,
                    status_id : req.body.status_id
                });
            }
        })
    }else{
        errors.insertError(res, 0, "No status id provide");
    }
}

/**
Des: get all status of user's friends on the basis of user id:
**/

function getStatusGallery(req, res){
    if(req.params.user_id){ /*Check for user id*/
        /*Check user Id exists in database*/
        Users.find({ _id : user_id }, function(uerr, srows){
            if(!uerr){
                

            }else{
                errors.insertError(res, 2, "User Id not exists"); 

            }
        });
    }else{
        errors.insertError(res, 0, "No user id provided");        
    }   
}


/**
Des: Removing Status using Status Id:
**/
funcs.removeStatus = function(req, res){
    if(req.body.status_id){
        UserStatus.find({ _id : req.body.status_id }, function(err, rows){
            if(!err){
                if(rows.length > 0){
                    UserStatus.remove({ _id : req.body.status_id }, function(err){
                        if(!err){
                            res.json({
                                        status : 1,
                                        msg : "Status removed",
                                        status_id: req.body.status_id
                            });
                        }
                    });
                }else{
                    errors.insertError(res, 2, "Status not exists!");
                }
            }else{
                errors.insertError(res, 2, "Status not exists!");
            }
        })
    }else{
        errors.insertError(res, 0, "No status id provided");
    }
}


funcs.deleteAllStatus = function(req, res){
    UserStatus.remove({}, function(err){
        PendingStatus.remove({}, function(a,b){
            UsersStatusList.remove({}, function(e,r){});
            MediaPending.remove({}, function(t,v){
                res.json({
                    status : 1
                });
            })
        });
    })
}

funcs.getAllStatus = function(req, res){
    UserStatus.find({}, function(err, rows){
        res.json(rows);
    })
}

funcs.getFriendsStatus = function(req, res){
    UsersStatusList.find({}, function(err, rows){
        res.json(rows);
    })
}

funcs.getPendingStatus = function(req, res){
    PendingStatus.find({}, function(err, rows){
        res.json(rows);
    })
}

funcs.getPendingMedia = function(req, res){
    MediaPending.find({}, function(err, rows){
        res.json(rows);
    })
}

function deActiveOldSatus(req){
    if(req.body.user_id){
        UserStatus.update({ user_id : req.body.user_id }, { is_active : 0 }, { multi : true }, function(err, row){
            if(!err){
                console.log("success");
            }
        });
    }else{
        console.log("failure");
    }
}

module.exports = funcs;