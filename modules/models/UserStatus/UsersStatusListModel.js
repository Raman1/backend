/**
 * Created by Horizon Softech on 12/17/2015.
 * Author  : Babar Bilal
 **/
var mongoose = require("../../db_connect");
var Schema = mongoose.Schema;

var UsersStatusListSchema = Schema({
    user_id : Schema.ObjectId,
    friends_status : []
});

var UsersStatusList = mongoose.model("UsersStatusList", UsersStatusListSchema);

module.exports = UsersStatusList;