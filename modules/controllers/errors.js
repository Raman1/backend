/**
 * Created by Horizon Softech on 1/14/2016.
 * Author  : Babar Bilal
 **/

var funcs = {};

funcs.insertError = function(res, code, msg){
    res.status(200).json({
        status : code,
        msg : msg
    });
}

module.exports = funcs;