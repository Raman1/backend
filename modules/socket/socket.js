/**
 * Created by NGTricks on 1/21/2016.
 * Author  : Babar Bilal
 **/

module.exports = function(serverSocket){
    var users = [];
    var pendingStatus = [];
    var pendingData = require("./pending");
    serverSocket.on("connection", function(socket){
        var getIdFlag = false;

        /*for(var k=0; k<users.length; k++){
            if(users[k].socket_id == socket.id){
                if(users[k].user_id.length == 0){

                }
            }
        }*/

        socket.emit("get_id", "");

        socket.on('disconnect', function(){
            removeUserFromSocket(socket.id);
        })

        socket.on('error', function(){
            console.log("error fired");
            removeUserFromSocket(socket.id);
        })

        /*socket.on('error', function(){
            removeUserFromSocket(socket.id);
        })*/

        socket.on('user_id', function(user_id){
            console.log("connected user", user_id);
            addUserToSocket(socket.id, user_id);
            pendingData.sendFriendRequests(socket, user_id);
            pendingData.sendPendingPersonalStatus(socket, user_id);
            pendingData.sendPendingAcceptedFriendRequests(socket, user_id);
            pendingData.sendPendingMedia(socket, user_id);
        });

        socket.on("error", function(data){
            console.log("Error", data);
        })

        socket.on("heartbeat", function(data){
            socket.emit("heartbeat", data);
        })

        //Remove Unknown user after 5 seconds
        //setTimeout(removeUnknownUser(socket.id), 5000);

        require("./friends")(serverSocket, socket, users);
        require("./status")(serverSocket, socket, users, pendingStatus);
    });

    function getIdOfAllSockets(){
        for(var key in serverSocket.sockets.sockets){
            if(serverSocket.sockets.sockets.hasOwnProperty(key)){
                serverSocket.sockets.sockets[serverSocket.sockets.sockets[key].id].emit("get_id", "");
            }
        }
    }

    function addUserToSocket(sid, uid){

        if(uid.length < 4){
            return;
        }
        var FindFlag = true;
        for(var i=0; i<users.length; i++){
            if(users[i].user_id == uid){
                users[i].socket_id = sid;
                FindFlag = false;
            }
        }
        if(FindFlag){
            users.push({
                socket_id : sid,
                user_id : uid
            });
        }
        console.log("UserConnected", users);
    }

    function removeUserFromSocket(socket_id){
        for(var i=0; i<users.length; i++){
            if(users[i].socket_id == socket_id){
                console.log("UserRemoved", users[i]);
                users.splice(i, 1);
                break;
            }
        }
    }

    setInterval(getIdOfAllSockets, 5000);

}