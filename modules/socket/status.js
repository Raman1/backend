/**
 * Created by Babar on 2/23/2016.
 */

module.exports = function(io, socket, users, pendingStatus){
    var Users = require("./../models/UserModels/UserModel");
    var UserFriends = require("./../models/UserModels/UserFriendsModel");
    var InviteFriends = require("./../models/UserModels/InviteFriendsModel");
    var UserStatus = require("./../models/UserStatus/UserStatusModel");
    var PendingStatus = require("./../models/UserStatus/PendingStatusModel");
    var UsersStatusList = require("./../models/UserStatus/UsersStatusListModel");
    var MediaPending = require("./../models/UserStatus/MediaPending");


    //Delete Status to Friends using Socket
    socket.on("remove_personal_status", function(data){
        var sockets = io.sockets.sockets;
        var myObject = {
            status_id: data.status_id
        };
        PendingStatus.remove({ status_id : data.status_id }, function(pp, kk){});
        UsersStatusList.remove({ friends_status : data.status_id }, function(ppp, kkk){});
            for(var i=0; i < data.t_users.length; i++){                    
                var SentFlag = false;
                for(var k=0; k<users.length; k++){
                    if(users[k].user_id == data.t_users[i]){
                        sockets[users[k].socket_id].emit("remove_personal_status", myObject);
                        console.log("remove_personal_status : ", myObject);
                        SentFlag = true;
                        break;
                    }
                }
            }
    });


    socket.on("new_personal_status", function(data){
        var sockets = io.sockets.sockets;
        socket.emit("received_personal_status", data);
        var cnt = 0;
        var sData = [];

        UserStatus.find({ _id : data.status_id }, function(serr, srow){
            if(srow.length > 0){
                srow = srow[0].toObject();
                srow.status_id = srow._id;
                delete srow.hide_from;

                for(var i=0; i<data.t_users.length; i++){
                    var sData = {
                        o_user : data.o_user,
                        t_user : data.t_users[i],
                        status_id : data.status_id
                    };

                    PendingStatus.remove({ t_user : data.t_users[i] }, function(ppp, kkk){});
                    var a = new PendingStatus(sData);
                    a.save(function(saverr, saverow){});
                    //pendingStatus.push(sData);

                    UsersStatusList.update({ user_id : data.t_users[i] }, {
                        user_id : data.t_users[i],
                        $addToSet : {
                            friends_status : data.status_id
                        }
                    },{
                        upsert : true
                    }, function(err, rows){});


                    var SentFlag = false;
                    for(var k=0; k<users.length; k++){
                        if(users[k].user_id == sData.t_user){
                            sockets[users[k].socket_id].emit("new_personal_status", srow);
                            console.log("Status one sent : ", srow.status_title);
                            SentFlag = true;
                            break;
                        }
                    }
                }
            }
        });

   });
    socket.on("verify_personal_received", function(data){
        PendingStatus.remove({
            status_id : data.status_id,
            t_user : data.user_id
        }, function(e){
            console.log("Pending Removed");
        });
    });

    socket.on("new_media", function(data){
        var sockets = io.sockets.sockets;
        if(data.status_id){
            socket.emit("received_media", data);
            console.log("rec media ", data);
            if(data.friends.length > 0){
                for(var i=0; i<data.friends.length; i++){
                    var thumb_url = "";
                    if(data.thumb_url){
                        thumb_url = data.thumb_url;
                    }
                    var a = new MediaPending({
                        status_id : data.status_id,
                        media_url : data.media_url,
                        media_type : data.media_type,
                        thumb_url : thumb_url,
                        friend_id : data.user_id,
                        t_user : data.friends[i]
                    });
                    a.save(function(e, row){
                        if(!e){
                                for(var k=0; k<users.length; k++){
                                if(users[k].user_id == row.t_user){
                                    sockets[users[k].socket_id].emit("new_media", row);
                                    console.log("sent media", row);
                                }
                            }
                        }
                    })
                }
            }
        }
    });

    socket.on("verify_media", function(data){
        console.log("PendingMediaRemoved", data);
        MediaPending.remove({ status_id : data.status_id, friend_id : data.friend_id }, function(a,b){});
    })
}