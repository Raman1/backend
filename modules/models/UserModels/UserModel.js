/**
 * Created by Horizon Softech on 12/17/2015.
 * Author  : Babar Bilal
 **/

var mongoose = require("../../db_connect");
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    f_name : String,
    l_name : String,
    gender : String,
    image : String,
    phone_number : String,
    country_code : String,
    email_address : String,
    created_at : { type : Date, default : Date.now },
    country : String,
    password : String,
    device_type : String,
    reg_id : String,
    thumbimage : String,
    full_phone_number : String
});

var User = mongoose.model("Users", UserSchema);

module.exports = User;