/**
 * Created by Horizon Softech on 12/17/2015.
 * Author  : Babar Bilal
 **/
var mongoose = require("../../db_connect");
var Schema = mongoose.Schema;

var PendingStatusSchema = Schema({
    o_user : Schema.ObjectId,
    t_user : Schema.ObjectId,
    status_id : Schema.ObjectId
});

var PendingStatus = mongoose.model("PendingStatus", PendingStatusSchema);

module.exports = PendingStatus;