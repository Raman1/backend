/**
 * Created by NGTricks on 1/14/2016.
 * Author  : Babar Bilal
 **/

var Users = require("./../models/UserModels/UserModel");
var UserFriends = require("./../models/UserModels/UserFriendsModel");
var InviteFriends = require("./../models/UserModels/InviteFriendsModel");
var UserStatus = require("./../models/UserStatus/UserStatusModel");
var PendingStatus = require("./../models/UserStatus/PendingStatusModel");
var UsersStatusList = require("./../models/UserStatus/UsersStatusListModel");
var errors = require("./errors");
var fs = require("fs");

var funcs = {};

funcs.addStatusImage = function(req, res){
    if(req.params.status_id){
        UserStatus.find({ _id : req.params.status_id }, function(err, row){
            if(!err){
                if(row.length > 0){
                    var uploadDir = makeMediaDirectory(row[0].user_id, req.params.status_id, req.baseURL);
                    req.busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
                        console.log("Image Upload", fieldname);
                        var newFilename = makeFileName(filename);
                        fstream = fs.createWriteStream(req.baseURL + "/" + uploadDir + newFilename);
                        file.pipe(fstream);
                        fstream.on('close', function () {
                            UserStatus.findByIdAndUpdate(req.params.status_id , {
                                $addToSet : {
                                    images : uploadDir + newFilename
                                }
                            }, {
                                upsert : true,
                                new : true
                            }, function(err, row){
                                if(!err){
                                    var rdata = {
                                        status : 1,
                                        local_id : req.params.local_id,
                                        status_id : req.params.status_id,
                                        media_url : uploadDir + newFilename,
                                        media_type : 1,
                                        is_active : row.is_active,
                                        hide_from : row.hide_from
                                    };
                                    if(req.params.local_sid){
                                        rdata.local_sid = req.params.local_sid
                                    }
                                    console.log("Image Upload done", fieldname);
                                    res.json(rdata);
                                }
                            });
                        });
                    });

                    req.busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
                        //console.log('Field [' + fieldname + ']: value: ' + inspect(val));
                    });

                    req.busboy.on('finish', function() {
                        //console.log('Done parsing form!');
                    });
                    req.pipe(req.busboy);
                }else{
                    errors.insertError(res, 2);
                }
            }else{
                errors.insertError(res, 3)
            }
        });
    }else{
        errors.insertError(res, 0);
    }
}

funcs.addStatusVideo = function(req, res){
    if(req.params.status_id){
        var files = 0;
        var video = {
            file : "",
            thumb : ""
        }
        UserStatus.find({ _id : req.params.status_id }, function(err, row){
            if(!err){
                if(row.length > 0){
                    var uploadDir = makeMediaDirectory(row[0].user_id, req.params.status_id, req.baseURL);
                    req.busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
                        var newFilename = makeFileName(filename);
                        console.log("Fieldname", fieldname);
                        console.log("Filename", filename);
                        fstream = fs.createWriteStream(req.baseURL + "/" + uploadDir + newFilename);
                        file.pipe(fstream);
                        fstream.on('close', function () {
                            if(fieldname == "file"){
                                video.file = uploadDir + newFilename;
                            }else{
                                video.thumb = uploadDir + newFilename;
                            }
                            if(files == 1){
                                UserStatus.findByIdAndUpdate(req.params.status_id , {
                                    $addToSet : {
                                        videos : video
                                    }
                                }, {
                                    upsert : true,
                                    new : true
                                }, function(err, row){
                                    if(!err){
                                        var rdata = {
                                            status : 1,
                                            local_id : req.params.local_id,
                                            is_active : row.is_active,
                                            status_id : req.params.status_id,
                                            media_url : video.file,
                                            media_thumb : video.thumb,
                                            media_type : 2,
                                            hide_from : row.hide_from
                                        };
                                        if(req.params.local_sid){
                                            rdata.local_sid = req.params.local_sid
                                        }
                                        console.log(rdata);
                                        console.log("Files", files);
                                        res.json(rdata);
                                    }
                                });
                            }
                            files++;
                        });
                    });

                    req.busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
                        //console.log('Field [' + fieldname + ']: value: ' + inspect(val));
                    });

                    req.busboy.on('finish', function() {
                        //console.log('Done parsing form!');
                    });
                    req.pipe(req.busboy);
                }else{
                    errors.insertError(res, 2);
                }
            }else{
                errors.insertError(res, 3)
            }
        });
    }else{
        errors.insertError(res, 0);
    }
}

function makeMediaDirectory(user_id, status_id, baseURL){
    var upDir = {};
    var date = new Date();

    upDir = makeFolder(baseURL, "/uploads/media/" + date.getFullYear())
    upDir = makeFolder(baseURL, upDir + (date.getMonth()+1));
    upDir = makeFolder(baseURL, upDir + (date.getDate()));
    upDir = makeFolder(baseURL, upDir + (date.getHours()));
    upDir = makeFolder(baseURL, upDir + (date.getMinutes()));
    return upDir;
}

function makeFolder(baseUrl, upDir){
    if(!fs.existsSync(baseUrl + upDir)){
        fs.mkdirSync(baseUrl + upDir);
    }
    return upDir + "/";
}

function makeFileName(filename){
    var date = new Date();
    date = new Buffer(date.toTimeString()).toString("base64");
    date = date.substr(0, date.length-1);

    filename = filename.split(".");
    filename = filename[filename.length-1];
    return (date + (Math.random() * 1000) + "." + filename);
}

module.exports = funcs;