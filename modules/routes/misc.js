/**
 * Created by Horizon Softech on 1/14/2016.
 * Author  : Babar Bilal
 **/

var express = require("express");
var Router = express.Router();
var country = require("./../controllers/countries");
var contacts = require("./../controllers/contacts");

Router.get("/getCountriesList", country.getCountriesList);
Router.post("/syncContacts", contacts.syncContacts);
Router.post("/addCountry", country.addCountry);


module.exports = Router;