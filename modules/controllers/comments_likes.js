/**
 * Created by NGTricks on 1/14/2016.
 * Author  : Babar Bilal
 **/

var Users = require("./../models/UserModels/UserModel");
var UserFriends = require("./../models/UserModels/UserFriendsModel");
var InviteFriends = require("./../models/UserModels/InviteFriendsModel");
var UserStatus = require("./../models/UserStatus/UserStatusModel");
var PendingStatus = require("./../models/UserStatus/PendingStatusModel");
var UsersStatusList = require("./../models/UserStatus/UsersStatusListModel");
var StatusCommentsLikes = require("./../models/UserStatus/StatusCommentsLikes");
var errors = require("./errors");

var funcs = {};

funcs.addComment = function(req, res){
    var data = req.body;
    if(data.status_id){
        UserStatus.find({ _id : data.status_id }, function(err, rows){
            if(!err){
                if(rows.length > 0){

                }
            }
        })
    }else{
        errors.insertError(res, 0, "No User Id Found!");
    }
}

module.exports = funcs;