/**
 * Created by NGTricks on 1/14/2016.
 * Author  : Babar Bilal
 **/

var express = require("express");
var Router = express.Router();
var status = require("./../controllers/status");
var statusMedia = require("./../controllers/statusMedia");

/*  Status Routes */
Router.post("/createStatus", status.createStatus);
Router.get("/deleteAllStatus", status.deleteAllStatus);
Router.get("/getAllStatus", status.getAllStatus);
Router.post("/removeStatus", status.removeStatus);
Router.get("/getFriendsStatus", status.getFriendsStatus);
Router.get("/getPendingStatus", status.getPendingStatus);
Router.get("/getPendingMedia", status.getPendingMedia);
Router.post("/removeOldActive", status.removeOldActive);
Router.post("/makeStatusDeActive", status.makeStatusDeActive);

/*  Media Routes */

Router.post("/addStatusImage/:status_id/:local_id", statusMedia.addStatusImage);
Router.post("/addStatusImage/:status_id/:local_id/:local_sid", statusMedia.addStatusImage);
Router.post("/addStatusVideo/:status_id/:local_id", statusMedia.addStatusVideo);
Router.post("/addStatusVideo/:status_id/:local_id/:local_sid", statusMedia.addStatusVideo);

module.exports = Router;