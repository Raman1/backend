/**
 * Created by Horizon Softech on 12/17/2015.
 * Author  : Babar Bilal
 **/
var mongoose = require("../../db_connect");
var Schema = mongoose.Schema;

var UserStatusSchema = Schema({
    user_id : Schema.ObjectId,
    f_name : String,
    l_name : String,
    status_type : String,
    status_text : String,
    status_title : String,
    start_time : String,
    time_duration : String,
    is_available : { type : Number, default : 1 },
    time_format : String,
    status : Number,
    is_active : { type : Number, default : 1 },
    personsal : { type : Number, default : 0 },
    pending_status : { type : Number, default : 2 },
    hide_from : {
        type : Array,
        default : []
    },
    is_expired : { type : Number, default : 0 },
    created_at : {
        type : Date,
        default : Date.now
    },
    images : {
        type : Array,
        default : []
    },
    videos : {
        type : Array,
        default : []
    }
});

var UserStatus = mongoose.model("UserStatus", UserStatusSchema);

module.exports = UserStatus;