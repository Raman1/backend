/**
 * Created by Babar on 2/28/2016.
 */

var funcs = {};
var Users = require("./../models/UserModels/UserModel");
var UserFriends = require("./../models/UserModels/UserFriendsModel");
var InviteFriends = require("./../models/UserModels/InviteFriendsModel");
var PendingStatus = require("./../models/UserStatus/PendingStatusModel");
var UserStatus = require("./../models/UserStatus/UserStatusModel");
var AcceptedFriendRequestsPending = require("./../models/UserModels/AcceptedFriendRequestsPending");
var MediaPending = require("./../models/UserStatus/MediaPending");

var SendRequestFlag = true;

funcs.sendFriendRequests = function (socket, user_id) {
    InviteFriends.find({ invite_to : user_id, pending_status : 1 }, function(err, rows){
        if(!err){
            if(rows.length > 0){
                for(var i=0; i<rows.length; i++){
                    var arrCnt = 0;
                    Users.find({ _id : rows[i].invite_by }, function(uerr, urows){
                        if(!uerr){
                            if(urows.length > 0){
                                socket.emit("new_friend_request", {
                                    status : 1,
                                    request_id : rows[arrCnt]._id,
                                    invite_to : rows[arrCnt].invite_to,
                                    invite_by : rows[arrCnt++].invite_by,
                                    f_name : urows[0].f_name,
                                    l_name : urows[0].l_name
                                });
                            }
                        }
                    })
                }
            }
        }
    })
}

funcs.sendPendingPersonalStatus = function (socket, user_id){
    PendingStatus.find({ t_user : user_id }, function(err, rows){
        if(!err){
            if(rows.length > 0){
                for(var i=0; i<rows.length; i++){
                    console.log("row", rows[i]);
                    UserStatus.find({ _id : rows[i].status_id }, function(e, row){
                        if(!e){
                            if(row.length > 0){
                                row = row[0].toObject();
                                row.status_id = row._id;
                                delete row.hide_from;
                                socket.emit("new_personal_status", row);
                                console.log("Socket",socket);
                                console.log("user_id",user_id);
//                                setTimeout(funcs.sendPendingMedia(socket, user_id), 2000);
                            }
                        }
                    });
                }
            }
        }
    })
}

funcs.sendPendingAcceptedFriendRequests = function(socket, user_id){
    AcceptedFriendRequestsPending.find({ send_to : user_id }, function(err, rows){
        if(!err){
            if(rows.length > 0){
                socket.emit("friend_req_accepted", rows[0]);
            }
        }
    })
}

funcs.sendPendingMedia = function(socket, user_id){
	console.log("sendPendingMedia   user_id",user_id);
    MediaPending.find({ t_user : user_id }, function(err, rows){
        if(!err){
        	if(rows.length > 0){
                for(var i=0; i<rows.length; i++){
                	console.log("Rows:",rows[i]);
                    socket.emit("new_media", rows[i]);
                }
            }
        }
    });
}

module.exports = funcs;