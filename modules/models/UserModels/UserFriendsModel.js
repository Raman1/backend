/**
 * Created by Horizon Softech on 12/17/2015.
 * Author  : Babar Bilal
 **/

var mongoose = require("../../db_connect");
var Schema = mongoose.Schema;

var UserFriendsSchema = Schema({
    user_id : Schema.ObjectId,
    Friends : { type : Array, default : []}
});

var UserFriends = mongoose.model("UserFriends", UserFriendsSchema);

module.exports = UserFriends;